﻿#include "stdafx.h"
#include <fstream>
#include <iostream>
#include <vector>
#include <time.h>
#include <algorithm>

using namespace std;

struct item
{
	int value;
	int weight;

	item( int value, int weight ): value( value ), weight( weight ) {};

};

vector<item> pool;
//fstream inputData( "1.txt" );
//fstream inputData( "knapsack1.txt" );
fstream inputData( "knapsack_big.txt" );
long long **map = new long long*[2];
int size;
int numItems;

void readInput();
void process1stLine( int pos );
void process2ndLine( int pos );

void readInput()
{
	int value, weight;
	while ( inputData >> value >> weight )
	{
		pool.push_back( item( value, weight ) );
	}
}

void process1stLine( int pos )
{
	item* curItem = &pool[pos - 1];
	long long exclude;
	long long include;
	for ( int capacity = 0; capacity < size; capacity++ )
	{
		if ( !( curItem->weight > capacity ) )
		{
			exclude = map[1][capacity];
			include = map[1][capacity - curItem->weight] + curItem->value;
			map[0][capacity] = exclude > include ? exclude : include;
		}
		else
		{
			map[0][capacity] = map[1][capacity];
		}
	}
}

void process2ndLine( int pos )
{
	item* curItem = &pool[pos - 1];
	long long exclude;
	long long include;
	for ( int capacity = 0; capacity < size; capacity++ )
	{
		if ( !( curItem->weight > capacity ) )
		{
			exclude = map[0][capacity];
			include = map[0][capacity - curItem->weight] + curItem->value;
			map[1][capacity] = exclude > include ? exclude : include;
		}
		else
		{
			map[1][capacity] = map[0][capacity];
		}
	}
}

void print1stLine()
{
	for ( int capacity = 0; capacity < size; capacity++ )
	{
		cout << map[0][capacity];
	}
	cout << endl;
}

void print2ndLine()
{
	for ( int capacity = 0; capacity < size; capacity++ )
	{
		cout << map[1][capacity];
	}
	cout << endl;
}

int _tmain( int argc, _TCHAR* argv[] )
{
	clock_t begin = clock();

	inputData >> size >> numItems;
	numItems++; // for the 0th row

	//Let A = 2 - D array
	map[0] = new long long[size];
	map[1] = new long long[size];

	//	Initialize A[0, x] = 0 for x = 0, 1, . . ., W
	for ( int pos = 0; pos < size; pos++ )
	{
		map[0][pos] = 0;
	}

	readInput();

	for ( int pos = 1; pos < numItems; pos++ )
	{
		if ( pos % 2 == 0 )
		{
			process1stLine( pos );
			//print1stLine();
		}
		else
		{
			process2ndLine( pos );
			//print2ndLine();
		}
	}

	cout << max( map[0][size - 1], map[1][size - 1] ) << endl;

	clock_t end = clock();
	double elapsed_secs = double( end - begin ) / CLOCKS_PER_SEC;
	cout << endl;
	cout << "Running Time: " << elapsed_secs; cout << endl;

	return 0;
}

